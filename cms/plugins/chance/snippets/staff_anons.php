<div class="container b_cntn__box_glr">
    <div class="row">
        <div class="col-xs-12">
            <div class="b_cntn__box_glr_tit">Кто будет осматривать</div>
            <div class="b_cntn__box_glr_wrap">

                <div class="col-lg-4 col-md-6 col-xs-12 vrac">
                    <center>
                        <img alt="" src="/images/doc-1.jpg"><br>

                        <h2 class="vraz">Терапевт</h2>

                        <div class="rekt2">Петросян Лилия Рафаэловна</div>
                    </center>
                </div>

                <div class="col-lg-4 col-md-6 col-xs-12 vrac">
                    <center>
                        <img alt="" src="/images/doc-2.jpg"><br>

                        <h2 class="vraz">Хирург</h2>

                        <div class="rekt2">Агаджанян Давид Зорикович</div>
                    </center>
                </div>

                <div class="col-lg-4 col-md-6 col-xs-12 vrac">
                    <center>
                        <img alt="" src="/images/doc-3.jpg"><br>

                        <h2 class="vraz">Невролог</h2>

                        <div class="rekt2">Михайлов Сергей Витальевич</div>
                    </center>
                </div>

                <div class="col-lg-4 col-md-6 col-xs-12 vrac">
                    <center>
                        <img alt="" src="/images/doc-4.jpg"><br>

                        <h2 class="vraz">Оториноларинголог</h2>

                        <div class="rekt2">Пушкарева Екатерина Ивановна</div>
                    </center>
                </div>

                <div class="col-lg-4 col-md-6 col-xs-12 vrac">
                    <center>
                        <img alt="" src="/images/doc-5.jpg"><br>

                        <h2 class="vraz">Офтальмолог</h2>

                        <div class="rekt2">Пыресин Дмитрий Викторович</div>
                    </center>
                </div>
            </div>
        </div>
    </div>
</div>