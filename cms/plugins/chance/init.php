<?php defined( 'SYSPATH' ) or die( 'No direct script access.' );

$plugin = Plugin::factory('chance', array(
	'title' => 'Медкомисия',
	'version' => '0.1',
	'description' => 'Плагин для работы проекта "Медкомисия". Выполнен в качестве тестового задания',
	'author' => 'Ilya Sereda'
))->register();

Assets_Package::add('chance');