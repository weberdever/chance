<?php defined('SYSPATH') or die('No direct script access.');

// Этот файл загружается после выполения SQL кода из файла install/shcema.sql
// Наличие данного файла не обязательно

$page_list = ORM::factory('page');
$page_list->status_id = Config::get('site', 'default_status_id');
$page_list->needs_login = Model_Page::LOGIN_INHERIT;
$page_list->published_on = date('Y-m-d H:i:s');

$page_data = array (
    'parent_id' => '1',
    'title' => 'назваыва',
    'slug' => 'personal',
    'redirect_url' => '',
    'breadcrumb' => 'назваыва',
    'meta_title' => 'назваыва',
    'meta_keywords' => '',
    'meta_description' => '',
    'robots' => 'INDEX, FOLLOW',
    'layout_file' => '0',
    'behavior_id' => '',
    'status_id' => '100',
    'password' => '',
    'published_on' => '2015-07-31 11:25:05',
    'needs_login' => '2',
);
$page_list = $page_list->values($page_data)->create();

$section = Datasource_Section::factory('hybrid');
$section_data = array(
    'name' => 'Персонал',
    'created_by_id' => Auth::get_id()
);
$ds_id = $section->create($section_data);

$widgets = array(
    array(
        'type' => 'hybrid_headline',
        'data' => array (
            'name' => 'Краткий список сотрудников',
            'frontend_template' => 'staff_anons',
            'caching' => 0,
            'cache_lifetime' => 3600,
            'cache_tags' => 'pages,page_parts,page_tags',
            'page_id' => $page_list->id,
            'ds_id' => $section->id(),
            'list_size' => 6,
            'throw_404' => 1,
            'only_published' => 1,
            'doc_uri' => '/personal/:id',
            'doc_id' => '',
        ),
        'blocks' => array (
            1 => 'extended'
        )
    )
);

foreach ($widgets as $widget) {
    Widget_Manager::install($widget);
}