<!DOCTYPE html>
<html lang="<?php echo I18n::lang(); ?>">
<head>
<?php
    echo Meta::factory($page)
            ->css('font_opensans', 'http://fonts.googleapis.com/css?family=Open+Sans:400,700,800&subset=cyrillic,latin')
            ->css('bootstrap', 'http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css')
            ->css('bootstrap.theme', PLUGINS_URL . 'chance/public/css/bootstrap-theme.min.css')
            ->css('index', PLUGINS_URL . 'chance/public/css/index.css')
            ->css('lightbox', PLUGINS_URL . 'chance/public/css/lightbox.css');
?>

    <style>
        .carousel-inner > .item > img,
        .carousel-inner > .item > a > img {
            width: 100%;
            margin: auto;
            height: 100%;
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#a").click(function () {
                var selected = $(this).attr('href');
                $.scrollTo(selected, 500);
                return false;
            });
        });
    </script>

</head>

<body>

<?php Block::run('header'); ?>

<?php Block::run('body'); ?>
<?php Block::run('extended'); ?>

<?php Block::run('footer'); ?>

</body>

<?php
echo Meta::factory()
         ->js('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js')
         ->js('jquery.easing', 'http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js', 'jquery')
         ->js('bootstrap', PLUGINS_URL . 'chance/public/js/bootstrap.min.js', 'jquery')
         ->js('lightbox', PLUGINS_URL . 'chance/public/js/lightbox.js', 'jquery')
         ->js('localscroll', PLUGINS_URL . 'chance/public/js/localscroll.js', 'jquery')
         ->js('scrollto', PLUGINS_URL . 'chance/public/js/scrollto.js', 'jquery')
?>

<script type="text/javascript">$(function ($) {
        $.localScroll({duration: 1000, hash: false });
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('a#go').click(function (event) {
            event.preventDefault();
            $('#overlay').fadeIn(400,
                function () {
                    $('#modal_form')
                        .css('display', 'block')
                        .animate({opacity: 1, top: '50%'}, 200);
                });
        });

        $('#modal_close, #overlay').click(function () {
            $('#modal_form')
                .animate({opacity: 0, top: '45%'}, 200,
                function () {
                    $(this).css('display', 'none');
                    $('#overlay').fadeOut(400);
                }
            );
        });
    });
</script>

</html>
